<?

function update_image( $user_image, $user_image_name, $user_image_type, $user_image_size,$user_dir)
{
include ("../library/config.php");


if (!is_dir($image_webdir.$user_dir))
{
	mkdir($image_webdir.$user_dir);
	$image_save_to = $image_webdir.$user_dir."/".$user_image_name;
}//end if is_dir
else
{
	if(file_exists($image_webdir.$user_dir."/".$user_image_name))
	{
		$user_image_name=explode(".",$user_image_name);
		$time=time(now);
		$image_save_to=$image_webdir.$user_dir."/".$user_image_name[0].$time.".".$user_image_name[1];
		if(file_exists($image_save_to))
		{
			update_image( $user_image, $user_image_name, $user_image_type, $user_image_size,$user_dir);
		}
		$user_image_name=implode(".",$user_image_name);
	}//end if file_exis	
	else
	{
		$image_save_to = $image_webdir.$user_dir."/".$user_image_name;
	}//end else file_exis
}//end else is dir
	if($user_image != "" && $user_image !='none')
	{
		if($user_image_size > $max_user_file_size)
		{
			echo "$user_image_name : Gambar yang anda berikan terlalu besar ukurannya. <br />
			Ukuran max yang diperbolehkan: $max_user_file_size bytes.<br />
			File anda: $user_image_size bytes. <br />";
			return false;
			exit;
		}//end if $user_image_size
		else{//else $user_image_size
			if(substr_count($allowed_types, $user_image_type) > 0)
			{
				if(copy($user_image, $image_save_to))
				return $image_save_to;
				else
				echo "$user_image_name : Gambar tidak dapat diload.  Coba lagi.<br />";
			}//end if substr
			
			else
			echo  "$user_image_name : Gambar yang anda masukkan salah tipe.<br />
			Tipe yang diperbolehkan adalah:$allowed_types<br />
			Tipe file anda: $user_image_type";
			}//end else $user_image
	}//end if image
	else
	{
		return false;
		
	}//end else image
}//end function
?>