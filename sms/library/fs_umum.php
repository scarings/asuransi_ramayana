<?php
/***********************************************
Nama File : fs_umum.php 
Fungsi    : Fungsi-fungsi untuk keperluan umum.

************************************************/
$reqfld = "<font color='#FF0000'> *) </font>";

/*function db_connect($con_db_database, $con_db_host, $con_db_name, $con_db_user, $con_db_passwd) {
    global $mdl;
    $strconn = $con_db_database."://".$con_db_user.":".$con_db_passwd."@".$con_db_host."/".$con_db_name;
    $db = DB::connect($strconn);
    if (DB::isError($db)) {
        tampilkan_error(999, $db->getMessage()."<br><br><b>Silakan hubungi Sistim Administrator!</b>");
    }
    return $db;
}*/

function set_status_text($str) {
    $strstatus  = " onMouseOver=\"window.status='".$str."';return true;\"";  
    $strstatus .= " onMouseOut=\"window.status='';return true;\"";
    return $strstatus;
}

function put_dropdown($name, $sql, $selval="", $isnotnull=false, $strnone="", $event="", $action="", $disabled=false) {
    global $db;

    $rslt = $db->query($sql); 
    if ($disabled) $strdisabled = "disabled"; 
    $strddl = "        <select $strdisabled name='$name' ";
    if ($event != "") {
        $strddl .= $event."=\"".$action."\"";
    }
    $strddl .= ">\n";
    if (!($isnotnull)) {
      if ($selval == "") {
        $strddl .= "          <option value='' selected>".$strnone."</option>\n";
        $selected = "";
      } else {
        $strddl .= "          <option value=''>".$strnone."</option>\n";
      }
    }
    while ($row = $rslt->fetchRow()) {
      if (($isnotnull) and ($selval=="")) {
        $selected = $row[0];
        $isnotnull = false;
        $selval = $row[0];
      }
      if (strlen($row[1]) > 50) 
        $strdisp = substr($row[1], 0, 50)."...";
      else
        $strdisp = $row[1];
      if ($row[0] == $selval) {
        $strddl .= "          <option value='".$row[0]."' selected>".$strdisp."</option>\n";
        $selected = $row[0];
      } else {
        $strddl .= "          <option value='".$row[0]."'>".$strdisp."</option>\n";
      }
    }
    $strddl .= "        </select>\n";
    echo $strddl;
    return $selected;
}

function put_array_dropdown($name, $arr, $selval="", $isnotnull=false, $strnone="", $event="", $action="", $disabled=false) {
    global $db;
    
    if ($disabled) $strdisabled = "disabled";
    $strddl = "        <select $strdisabled name='$name' ";
    if ($event != "") {
        $strddl .= $event."=\"".$action."\"";
    }
    $strddl .= ">\n";
    if (is_array($arr)) {
      if (!$isnotnull) {
        if ($selval == "") {
          $strddl .= "          <option value='' selected>".$strnone."</option>\n";
          $selected = "";	    
        } else {      
          $strddl .= "          <option value=''>".$strnone."</option>\n";    
        }
      } 
		  foreach ($arr as $key => $value) {
        if (($isnotnull) and ($selval=="")) {
          $selected = $key;
          $isnotnull = false;
          $selval = $key;          
        }
        if (strlen($value) > 50) 
          $strdisp = substr($value, 0, 50)."...";
        else
          $strdisp = $value;
        
		    if ($key == $selval) {			 		 			 
          $strddl .= "          <option value='".$key."' selected>".$strdisp."</option>\n";
          $selected = $key;	
		    } else {
          $strddl .= "          <option value='".$key."'>".$strdisp."</option>\n";			 
		    }			 
		  }
    }
    $strddl .= "        </select>\n";
    echo $strddl;
    return $selected;
}

function put_datefield($fldname,$dflt='',$offset1='1',$offset2='1',$bl='P') {
    $tgl = "tgl_".$fldname;
    $bln = "bln_".$fldname;
    $thn = "thn_".$fldname;
    list($dflt_y,$dflt_m,$dflt_d)=split("-",$dflt);
    echo "<input type=hidden name='$fldname' value='$dflt'>\n";
    echo "<select name='$tgl' onChange=onChangedatefld($fldname,$tgl,$bln,$thn)>\n";
    echo "<option value=''> </option>\n";
    for ($i=1; $i<=31; $i++){
        if ($i == $dflt_d) {
            echo "<option value=$i selected> $i</option>\n";
        } else {
            echo "<option value=$i> $i</option>\n";
        }
    }
    echo "</select>\n";
    echo "<select name='$bln' onChange=onChangedatefld($fldname,$tgl,$bln,$thn)>\n";
    echo "<option value=''> </option>\n";
    for ($i=1; $i<=12; $i++){
        if ($i == $dflt_m) {
            echo "<option value=$i selected> ".namabulan($bl,$i)."</option>\n";
        } else {
            echo "<option value=$i> ".namabulan($bl,$i)."</option>\n";
        }
    }
    echo "</select>\n";
    echo "<select name='$thn' onChange=onChangedatefld($fldname,$tgl,$bln,$thn)>\n";
    echo "<option value=''> </option>\n";
    for ($i=(date("Y")-$offset1); $i<=(date("Y")+$offset2); $i++){
        if ($i == $dflt_y) {
            echo "<option value=$i selected>$i</option>\n";
        } else {
            echo "<option value=$i>$i</option>\n";
        }
    }
    echo "</select>\n";
}

function tampilkan_error($errno, $errtext){
    global $webpath;
    if(!defined("TOOLBAR_INC"))
        include('toolbar.php');
    global $ss_css;
    echo "<html>\n<head>\n";
    echo "<link rel='stylesheet' type='text/css' href='$webpath/css/default.css'>\n";
    echo "</head>\n<body>\n";
    $tb = new Toolbar;
    $tb->showToolbar("&nbsp;");
    echo "<table border='0' width='100%'>\n";
    echo "<tr>\n";
    echo "<td width='3%'><img src='$webpath/images/icon_error.gif' border='0'></td>\n";
    echo "<td class=judul valign='middle'>E R R O R</td>\n";
    echo "</tr>\n";
    echo "<tr>\n";
    echo "<td colspan='2'>&nbsp;</td>\n";
    echo "</tr>\n";
    echo "<tr>\n";
    echo "<td colspan='2'>\n";
    if ($errno <> "") {
        echo "<b>No. : </b>".$errno."<br>\n";
        echo "<b>Error : </b>".$errtext."\n";
    } else {
        echo "Error : ".$errtext."\n";
    }
    echo "</td>\n";
    echo "</tr>\n";
    echo "</table>\n";
    echo "</body>\n</html>";
    exit -1;
}

function exec_sqltext($db,$sqltext) {
    $comm = $db->query($sqltext);
    check_db_error($comm);
}

function check_db_error($obj) {
    if (DB::isError($obj)) {
        $errno = $obj->getcode();
        $errtext = DB::errorMessage($errno);
        if ($errno != -5) {
            tampilkan_error($errno, $errtext);
        }
    }
}

function navigasi_halaman($url,$halaman,$ttlperhalaman,$jmldata) {
    $jmlhalaman = ceil($jmldata/$ttlperhalaman);
    if ($jmlhalaman == 0) $jmlhalaman = 1;
    if ($jmldata > $ttlperhalaman) {
       echo "  <table width='100%' border=0>\n";
       echo "    <tr>\n";
       echo "      <td align='center'><font size=2><b>\n";
       if ($halaman > 1) {
          $hal_sblm = $halaman - 1;
          if (ereg("\?",$url)) {
              $url1 = $url."&hal=".$hal_sblm;
          } else {
              $url1 = $url."?hal=".$hal_sblm;
          }
          echo "      <a href='".$url1."' > < </a> \n";
       } else {
          echo "<\n";
       }
       echo " Hal: ".$halaman." / ".$jmlhalaman. "\n";
       if ($halaman*$ttlperhalaman < $jmldata) {
          $hal_ssdh = $halaman + 1;
          if (ereg("\?",$url)) {
              $url2 .= $url."&hal=".$hal_ssdh;
          } else {
              $url2 .= $url."?hal=".$hal_ssdh;
          }
          echo "      <a href='".$url2."' > > </a> \n";
       } else {
          echo ">\n";
       }
       echo "      </b></font></td>\n";
       echo "    </tr>\n";
       echo "  </table><p>\n";
    }
}

function rupiah_terbilang($val) {
    $vtemp = explode(".",number_format($val,0,',','.'));
    $se = array("puluh","belas","ratus","ribu");
    for($k=0; $k<count($vtemp); $k++) {
        $j = 0;
        $v = $vtemp[$k];
        $satuan = $satuan1 = $bilangan1 = $return1 = "";
        if ($k == 0) {
            $digits = strlen($val);;
        } else {
            $digits = strlen($val)-strlen($vtemp[0])-(3*($k-1));
        }
        if ($digits > 3) $satuan = satuan($digits);
        for ($i=strlen($v); $i>0; $i--) {
            $bil = substr($v,$j++,1);
            $satuan1 = satuan($i);
            $bilangan1 = bilangan($bil);
            if ($bil == 1 and $i == 2) {
                $bil = substr($v,$j++,1);
                if ($bil > 0) {
                    $bilangan1 = bilangan($bil);
                    $satuan1 = "belas";
                }
                $i--;
            }
            if ($bilangan1 == "satu" and in_array($satuan1,$se)) {
                $return1 .= "se".$satuan1." ";
            } elseif ($bil > 0) {
                $return1 .= $bilangan1." ".$satuan1." ";
            }
        }
        if (rtrim($return1) == "satu" and in_array($satuan,$se)) {
            $return .= "se".$satuan." ";
        } elseif ($return1 != "") {
            $return .= $return1." ".$satuan." ";
        }
    }
    return ucwords("$return rupiah");
}

function bilangan($val) {
    switch ($val) {
        case 1: return "satu";     break;
        case 2: return "dua";      break;
        case 3: return "tiga";     break;
        case 4: return "empat";    break;
        case 5: return "lima";     break;
        case 6: return "enam";     break;
        case 7: return "tujuh";    break;
        case 8: return "delapan";  break;
        case 9: return "sembilan"; break;
        default: return "";
    }
}

function satuan($digits) {
    if ($digits == 2) {
        return "puluh";
    } elseif ($digits == 3) {
        return "ratus";
    } elseif ($digits > 3 and $digits <=6 ) {
        return "ribu";
    } elseif ($digits > 6 and $digits <=9 ) {
        return "juta";
    } elseif ($digits > 9 and $digits <=12 ) {
        return "milyar";
    } elseif ($digits > 12 and $digits <=15 ) {
        return "trilyun";
    } else {
        return "";
    }
}

function cut_text($str, $length) {
   if (strlen($str) > $length) {
      return substr($str, 0, $length)."..."; 
   } else {
      return $str;
   }
}

function foot_note($txt) {
   echo "<table cellpadding=7 width=100% border=0 cellspacing=0>";
   echo "<tr>";	
   echo "<td>$txt</td>";		 
   echo "</tr>";		 
   echo "</table>";	 
}

// get all menu item records from table appl_menu_item
// put them all in an associativ array ($items)
// this function runs recursivly (!) to get all menu items and all submenus
function getMenuItems(&$items, $idmenu, $grup) {
    global $db;
    
    if (empty($items)) {
        $level = 0;
    } else {
        $level = $items[sizeof($items)-1]["level"] + 1;
    }
    $sql  = "SELECT DISTINCT seq, prompt, id_menu_item, dokumen, parameter, appl_menu_item.modul, submenu, ";
    $sql .=        "appl_grup_menuitem_akses.tipe_akses as tipe_akses ";
    $sql .=   "FROM appl_menu_item LEFT JOIN appl_grup_menuitem_akses ";
    $sql .=           "ON id_menu_item = menu_item and appl_grup_menuitem_akses.nama_grup = '$grup' ";
    $sql .=    "WHERE appl_menu_item.menu = '".$idmenu."' "; 
    $sql .=  "ORDER BY seq"; 
    $rslt = $db->query($sql); 

    $i = sizeof($items);
    while ($row = $rslt->fetchRow(DB_FETCHMODE_ASSOC)) {
        $items[$i]['level'] = $level;   
        $items[$i]['prompt'] = $row["prompt"];
        $items[$i]['iditem'] = $row["id_menu_item"];
        $items[$i]['document'] = $row["dokumen"];        
        $items[$i]['param'] = $row["parameter"];
        $items[$i]['modul'] = $row["modul"];
        $items[$i]['access'] = $row["tipe_akses"];          
        if (!empty($row["submenu"])) {
            $items[$i]['submenu'] = $row["submenu"];
            $i = getMenuItems($items, $row["submenu"], $grup);
        } else {
            $items[$i]['submenu'] = '';
            $i++;
        }
    }
    return sizeof($items);
}


function get_sequence ($tbl) {
    global $db;
    $seq = $db->getOne("select seq from $tbl");
    $nextseq = $seq + 1 ;
    $res = $db->query("update $tbl set seq = '$nextseq'");
    if (DB::isError($res)) {
        $errno = $res->getcode();
        $errtext = DB::errorMessage($errno);
        $errtext .= "SQL Text : <br>$sql<br>\n";
        tampilkan_error($errno, $errtext);
    }
    ob_end_clean();
    return $seq;
}

class SqlLog {
    var $id;
    var $sql;
    
    function SqlLog ($aa) {
        foreach ($aa as $k=>$v)
            $this->$k = $aa[$k];
    }
}

function readLogXML($filename) {
    // read the xml database of aminoacids
    $data = implode("",file($filename));
    $parser = xml_parser_create();
    xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
    xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
    xml_parse_into_struct($parser,$data,$values,$tags);
    xml_parser_free($parser);

    // loop through the structures
    foreach ($tags as $key=>$val) {
        if ($key == "log") {
            $logranges = $val;
            for ($i=0; $i < count($logranges); $i+=2) {
                $offset = $logranges[$i] + 1;
                $len = $logranges[$i + 1] - $offset;
                $tdb[] = parseLog(array_slice($values, $offset, $len));
            }
        } else {
            continue;
        }
    }
    return $tdb;
}

function parseLog($mvalues) {
    for ($i=0; $i < count($mvalues); $i++)
        $log[$mvalues[$i]["tag"]] = $mvalues[$i]["value"];
    return new SqlLog($log);
}

function set_tabstrip($arrtabstrip, $activetab = 0, $height = 50) {
    global $webpath;
   
    $pix1 = "$webpath/images/pixel1x1.gif"; 
    $content = call_user_func_array($arrtabstrip[$activetab]["func"], $arrtabstrip[$activetab]["param"]);
    
    echo "<table border='0' width='100%' cellpadding='0' cellspacing='0'>\n";
    echo "  <tr>\n";
    echo "    <td>\n";
    //echo "    <span class='display_only'>\n";
    // create tabstrips
    echo "      <table width='100%' border='0' height='21' cellpadding='0' cellspacing='0'>\n";
    echo "        <tr>\n";
    for ($i=0; $i<sizeof($arrtabstrip); $i++) {
        $width_strip = (isset($arrtabstrip[$i]["width"])) ? ($arrtabstrip[$i]["width"]) : 100;
        echo "          <td width='1' height='1'></td>\n"; // left-top corner
        echo "          <td width='".$width_strip."' height='1' background='$pix1'></td>\n"; // top horz. line
        echo "          <td width='1' height='1' background='$pix1'></td>\n"; // right-top corner
        echo "          <td width='1' height='1'></td>\n"; // space
    } 
    echo "          <td height='1'></td>\n"; // blank, not a tabstrip
    echo "        </tr>\n";
    echo "        <tr>\n";
    for ($i=0; $i<sizeof($arrtabstrip); $i++) {    
        echo "          <td width='1' height='25' background='$pix1'></td>\n"; // left vert. line
        if ($i == $activetab) {
            echo "          <td height='25' align='center' class='active_tabstrip'>\n";
            echo "          ".$arrtabstrip[$i]["title"]."\n"; // tabstrip-title
            echo "          </td>\n";            
        } else {
            echo "          <td height='25' align='center' class='inactive_tabstrip'>\n";
            echo "          <a href=\"javascript:".$arrtabstrip[$i]["action"]."\">\n";
            echo "            ".$arrtabstrip[$i]["title"]."\n"; // tabstrip-title
            echo "          </a>\n";            
            echo "          </td>\n";          
        }       
        echo "          <td width='1' height='25' background='$pix1'></td>\n"; // right vert. line
        echo "          <td width='1' height='25'></td>\n"; // space
    }
    echo "          <td height='25'>&nbsp;</td>\n"; // blank, not a tabstrip
    echo "        </tr>\n";
    echo "        <tr>\n";
    for ($i=0; $i<sizeof($arrtabstrip); $i++) {  
        $width_strip = (isset($arrtabstrip[$i]["width"])) ? ($arrtabstrip[$i]["width"]) : 100;  
        // active tabstrip   
        if ($i == $activetab) {
            echo "          <td width='1' height='1' background='$pix1'></td>\n"; // left-bottom corner
            echo "          <td width='".$width_strip."' height='1' class='active_tabstrip'></td>\n"; // bottom line
            echo "          <td width='1' height='1'></td>\n"; // right-bottom corner
            echo "          <td width='1' height='1' background='$pix1'></td>\n"; // space
        } else {   
        // inactive tabstrip
            echo "          <td width='1' height='1' background='$pix1'></td>\n"; // left-bottom corner
            echo "          <td width='".$width_strip."' height='1' background='$pix1'></td>\n"; // bottom line
            echo "          <td width='1' height='1' background='$pix1'></td>\n"; // right-bottom corner
            echo "          <td width='1' height='1' background='$pix1'></td>\n"; // space
        }
    }
    echo "          <td height='1' background='$pix1'></td>\n";  // blank, not a tabstrip
    echo "        </tr>\n";
    echo "      </table>\n";
    //echo "    </span>\n";
    //echo "    <span class='printer_only'><b>".$arrtabstrip[$activetab]["title"]."</b><hr size='1' noshade></span>\n";
    echo "    </td>\n";
    echo "  </tr>\n";
    echo "  <tr>\n";
    echo "    <td height='$height'>\n";
    // create tabstrip-content
    echo "      <table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0' class='active_tabstrip'>\n";
    echo "        <tr>\n";
    echo "          <td width='1' background='$pix1'></td>\n";
    echo "          <td>\n";
    echo "            <table width='100%' cellpadding='10'>\n";
    echo "              <tr><td>\n\n";
    echo "      <!-- Start Tabstrip content -->\n";
    echo "              ".$content."\n";
    echo "      <!-- End Tabstrip content -->\n\n";
    echo "              </td></tr>\n";
    echo "            </table></td>\n"; // write content here
    echo "          <td width='1' background='$pix1'></td>\n";
    echo "        </tr>\n";
    echo "        <tr>\n";
    echo "          <td width='1' height='1' background='$pix1'></td>\n";
    echo "          <td height='1' background='$pix1'></td>\n";
    echo "          <td width='1' height='1' background='$pix1'></td>\n";
    echo "        </tr>\n";
    echo "      </table>\n";
    echo "    </td>\n";
    echo "  </tr>\n";
    echo "</table>\n";
}

function text2tbl($filename, $separator, $table) {
    global $db;
    
    $fp = @fopen($filename, "r");
    if (!$fp) tampilkan_error(999, "File $filename tidak ditemukan");
    $i = 0;
    while (!feof($fp)) {
        $buf = fgets($fp, 4096);
        if ($i == 0) {            
            $keys = explode(";", trim($buf));
        } else {
            $vals = explode(";", trim($buf));
            if (sizeof($keys) == sizeof($vals)) {
                for ($j=0; $j<sizeof($keys); $j++) {
                    $flds[$keys[$j]] = $vals[$j]; 
                }
                insert_data($table, $flds, false);
            }
        }
        $i++;
    }
}


function login() {
//do login stuff
$_SESSION["timer"]=time();
}

function logout() {
session_start();
session_unset();
session_destroy();
header("location: login.php");
}

function sessi(){

$_SESSION["login"];
$user = $_SESSION["active_group"];
$display= $_SESSION["menus"];
$url= $_SESSION["tag"];

}

function encryptLink($val1, $param, $page){
    $keySalt = "infomedia nusantara";  
    $qryStr = $param."=".$val1;  
    $query = base64_encode(urlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($keySalt), $qryStr, MCRYPT_MODE_CBC, md5(md5($keySalt)))));  
    $link = $page."?".$query;
    return $link;
}
?>
