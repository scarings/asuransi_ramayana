<?php
define("TOOLBAR_INC", "toolbar.php");
define("IMG_BUTTON_BACK",   "images/button_back.gif:kembali:0");
define("IMG_BUTTON_RELOAD", "images/button_reload.gif:refresh:1");
define("IMG_BUTTON_LIST",   "images/button_list.gif:list:2");
define("IMG_BUTTON_SAVE",   "images/button_save.gif:simpan:3");
define("IMG_BUTTON_EDIT",   "images/button_edit.gif:edit:4");
define("IMG_BUTTON_INPUT",  "images/button_input.gif:input:5");
define("IMG_BUTTON_DELETE", "images/button_delete.gif:hapus:6");
define("IMG_BUTTON_CLEAR",  "images/button_clear.gif:kosongkan:9");
define("IMG_BUTTON_EXCEL",   "images/button_excel.gif:bantuan:7");
define("IMG_BUTTON_PRINT",  "images/button_print.gif:cetak:8");
define("IMG_BUTTON_HELP",   "images/button_help.gif:bantuan:9");

define("STR_CONFORM_DELETE", "Apakah data tersebut benar-benar akan dihapus?");

class Toolbar {
    var $elements;
    var $strHtml;
    var $tbLen;
    var $strCaption;
    var $btnListDisabled = false;
    var $btnHelpDisabled = false;
	var $btnPrintEnabled = false;
	    
	
    function Toolbar() {
        global $PHP_SELF, $mode;
		
        $this->tbLen = 0;
		$this->addButton(IMG_BUTTON_BACK, "javascript:window.history.go(-1);"); 
		$this->addButton(IMG_BUTTON_RELOAD, "javascript:window.location.reload();");
		$this->addSeparator(); 
        if (!empty($mode)) {
            $this->addButton(IMG_BUTTON_LIST, $PHP_SELF);
        }       
    }
       
    function addSeparator() {
        $this->elements[$this->tbLen][0] = -1; 
        $this->tbLen++;
    }
   
    function addButton($image, $link, $evt="", $act_evt="", $target="") {
        list($this->elements[$this->tbLen]['img'], $this->elements[$this->tbLen]['alt'], $seq) = split(':', $image);
        $this->elements[$this->tbLen]['seq'] = $seq; //untuk urutan keberapa imagenya
        $this->elements[$this->tbLen]['act'] = $link; //untuk linknya kemana
        $this->elements[$this->tbLen]['evt'] = $evt;
        $this->elements[$this->tbLen]['act_evt'] = $act_evt;
		$this->elements[$this->tbLen]['tgt'] = $target; //untuk targetnya kemana               
        $this->tbLen++;
    }

    function addButtonEdit($post, $evt="", $act_evt="") {
        if (ereg("'", $post)) {
            $this->addButton(IMG_BUTTON_EDIT, "javascript:window.open($post, 'Content');", $evt, $act_evt);
        } else {
            $this->addButton(IMG_BUTTON_EDIT, "javascript:window.open('$post', 'Content');", $evt, $act_evt);
        }
    }
        
    function addButtonInput($post, $evt="", $act_evt="") {
        if (ereg("'", $post)) {
            $this->addButton(IMG_BUTTON_INPUT, "javascript:window.open($post, 'Content');", $evt, $act_evt);
        } else {
            $this->addButton(IMG_BUTTON_INPUT, "javascript:window.open('$post', 'Content');", $evt, $act_evt);
        }
    }

    function addButtonDelete($form = "forms[0]") {
        //$this->addButton(IMG_BUTTON_DELETE, "javascript:document.".$form.".submit();");
        $this->addButton(IMG_BUTTON_DELETE, "javascript: if (window.confirm('".STR_CONFORM_DELETE."')) document.".$form.".submit();");
    }

    function addButtonSave($form = "forms[0]", $lnk = "javascript: document.forms[0].submit();", $evt="", $act_evt="") {
        $this->addButton(IMG_BUTTON_SAVE, "javascript: document.".$form.".submit();", $evt, $act_evt);
    }

    function addButtonClear($form = "forms[0]") {
        $this->addButton(IMG_BUTTON_CLEAR, "javascript:document.".$form.".reset();");
    }
      
    function addButtonList() {
            global $PHP_SELF;
            $this->addButton(IMG_BUTTON_LIST, "javascript:window.open('$PHP_SELF', 'Content');", $evt, $act_evt);
    }

    function addButtonPrint() {
        $this->addSeparator(); 
        $this->addButton(IMG_BUTTON_PRINT, "javascript:window.print();");
    }

    function addButtonExcel($post, $evt="", $act_evt="") {
        if (ereg("'", $post)) {
            $this->addButton(IMG_BUTTON_EXCEL, "javascript:window.open($post, 'Content');", $evt, $act_evt);
        } else {
            $this->addButton(IMG_BUTTON_EXCEL, "javascript:window.open('$post', 'Content');", $evt, $act_evt);
        }
    }

    function showToolbar($caption = "") {
        global $webpath;
		$this->strCaption = $caption;
		if($this->btnPrintEnabled) {
			$this->addButtonPrint();
		}
		if (!$this->btnHelpDisabled) {        
			$this->addButton(IMG_BUTTON_HELP, "$webpath/help/", "", "", "new");
  		}
        $this->strHtml .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
        $this->strHtml .= " <tr>\n";
        $this->strHtml .= "   <td width='10' height='40'><img class='display_only' border='0' src='$webpath/images/bar_left.gif' width='10' height='40'></td>\n";
        $this->strHtml .= "   <td valign='middle' height='20' background='$webpath/images/bar_mid.gif' style='padding-bottom:5px'>\n";        
        $this->strHtml .= "     <table class='display_only' border='0' cellpadding='0' cellspacing='0'>\n";
        $this->strHtml .= "       <tr>\n";
        for($i=0; $i<$this->tbLen; $i++) {
//            $image = $this->elements[$i][0];

            if (!isset($this->elements[$i]['img'])) {
                $this->strHtml .= "         <td width='5'>\n";
                $this->strHtml .= "         </td>\n";            
            } else {
                if (($this->btnListDisabled) && (ereg($this->elements[$i]['img'], IMG_BUTTON_LIST)))
                    continue;
                $this->strHtml .= "         <td width='25'>\n";               
                $this->strHtml .= "           <a href=\"".$this->elements[$i]['act']."\" ".set_status_text($this->elements[$i]['alt'])." ";
                if ($this->elements[$i]['evt'] != "")
                    $this->strHtml .= $this->elements[$i]['evt']."=\"".$this->elements[$i]['act_evt']."\"";
                $this->strHtml .= " target=\"".$this->elements[$i]['tgt']."\">\n";
                $this->strHtml .= "             <img src='".$webpath."/".$this->elements[$i]['img']."' ";
                $this->strHtml .= "alt='".$this->elements[$i]['alt']." ' border='0'>\n";
                $this->strHtml .= "           </a>\n";
                $this->strHtml .= "         </td>\n";                
            }  
        }
        $this->strHtml .= "       </tr>\n";
        $this->strHtml .= "     </table>\n";   
        $this->strHtml .= "   </td>\n";
        $this->strHtml .= "   <td height='20' border='0' valign='middle' background='$webpath/images/bar_mid.gif' style='padding-bottom:5px'>\n";        
        $this->strHtml .= "     <table width='100%' border='0'>\n";
        $this->strHtml .= "       <tr>\n";
        $this->strHtml .= "         <td class='judul' valign='bottom'>\n";
        $this->strHtml .= "           <p align='right'>".$caption."</p>\n";
        $this->strHtml .= "         </td>\n";     
        $this->strHtml .= "       </tr>\n";
        $this->strHtml .= "     </table>\n";
        if (!empty($caption))
            $this->strHtml .= "     <hr class='printer_only' size='1' valign='top' noshade>\n";
        $this->strHtml .= "   </td>\n";
        $this->strHtml .= "   <td width='10' height='40'><img class='display_only' border='0' src='$webpath/images/bar_right.gif' width='10' height='40'></td>\n";        
        $this->strHtml .= "  </tr>\n";  
        $this->strHtml .= "</table><br>\n";  
     
        return $this->strHtml;
        //echo $this->strHtml;
    }   

    function showBottomToolbar() {
        global $webpath;
        
        $this->strHtml = "<table align='center' border='0' cellpadding='0' cellspacing='0'>\n";
        $this->strHtml .= "  <tr>\n";
        $this->strHtml .= "    <td>\n";        
        for($i=0; $i<$this->tbLen; $i++) {
            $image = $this->elements[$i][0];
            if ($this->elements[$i][0] != -1) {             
                $this->strHtml .= "      &nbsp;[<a href=\"".$this->elements[$i]['act']."\" ".set_status_text($this->elements[$i]['alt']).">";
                $this->strHtml .= $this->elements[$i]['alt']."</a>]&nbsp;\n";
            }  
        }
        $this->strHtml .= "    </td>\n";
        $this->strHtml .= "  </tr>\n";
        $this->strHtml .= "</table>\n";  
        $this->strHtml .= "<hr>\n";             
        
		return $this->strHtml;      
        //echo $this->strHtml;
    }   
    
    function disableButtonList() {
        $this->btnListDisabled = true;
    }
    
    function disableButtonHelp() {
        $this->btnHelpDisabled = true;
    }

	function enableButtonPrint() {
        $this->btnPrintEnabled = true;
    }

    function setStepButtonBack($step) {
        $this->elements[0]['act'] = "javascript:parent.content.history.go(-$step);";
    }
}
?>
