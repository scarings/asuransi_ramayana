<?php
//--- database connection
$db_conn["host"] = "localhost";		// database server
$db_conn["user"] = "sms";			// login database server
$db_conn["passwd"] = "sms";	      // password to database server YWRtaW5vbmx5
$db_conn["db_name"] = "sms_inbound";		// nama database default
//-----------------------



//----- set env
$max_user_file_size = "50000";
$image_webdir = "../userimages/";
$max_msg=50;
$root = "..";
$class_path = "../classes";
$lib_path = "../library";
$image_path = "../images";
$template_path = "../app_templates";
$webpath="..";
if(stristr(ini_get("include_path"),";")) $ini_lim = ";";
else $ini_lim = ":";
ini_set("include_path",".".$ini_lim.$class_path);
session_save_path("../session");
require_once("add_function.php");
//----- end set env

$row_count = 25;
$page_count = 10;
$title="CallCenter 108  :: PT. Infomedia Nusantara";
//$genap="#ebebeb";//#EBF2FE";
$genap="#F4F8FF";
$ganjil="#FFFFFF";

$normal_color = "#FFFFFF";
$over_color = "#CCCCCC";
$mark_color = "#FFCC99";
$max_img_size = "50000";
$userimage = "../userimages";
$allowed_types = "image/jpeg, image/gif, image/pjpeg, image/x-png";
?>