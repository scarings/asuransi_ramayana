<?
class send_mail
{

	var $to = array();
	var $cc = array();
	var $bcc = array();
	var $from_name = "";
	var $from_mail = "";
	var $subject = "";
	var $Body = "";
	var $AltBody = "";
	var $mailer = "Send Mail";
	var $priority = 3;
	var $header = "";
	var $attachment = "";
    var $CharSet = "iso-8859-1";

	function send_mail()
	{
		$this->boundary = "_b".md5(uniqid(time()));
		$this->subboundary = "_sb".md5(uniqid(time()));
	}

	function send()
	{
		$to = $this->create_to($this->to);
		$body = $this->create_body();
		$header = $this->create_header();
		$result = @mail($to, $this->subject, $body, $header);
		if(!$result) $print_result = false;
		else $print_result = true;
		return $print_result;
	}

	function add_to($to_address)
	{
		$this->to[] = trim($to_address);
	}

	function add_cc($cc_address)
	{
		$this->cc[] = trim($cc_address);
	}

	function add_bcc($bcc_address)
	{
		$this->bcc[] = trim($bcc_address);
	}

	function add_attach($path,$name = "",$encoding = "base64",$type = "application/octet-stream")
	{
		if(@is_file($path))
		{
			if(!$name) $name = basename($path);
		}
		$this->attachment .= "--Boundary-=".$this->boundary."\r\n";
		$this->attachment .= "Content-Type: ".$type."; ";
		$this->attachment .= "name=\"".$name."\"\r\n";
		$this->attachment .= "Content-Transfer-Encoding: ".$encoding."\r\n";
		$this->attachment .= "Content-Disposition: attachment; filename=\"".$name."\"\r\n\r\n";
		$fp = fopen($path,"rb");
		$content = fread($fp,filesize($path));
		fclose($fp);
		$this->attachment .= $this->string_encode($content,$encoding)."\r\n\r\n";
	}
	
	function string_encode($content,$encoding)
	{
		switch(strtolower($encoding))
		{
			case "base64" : $result = chunk_split(base64_encode($content));
							break;
			case "binary" : $result = $content; break;
		}
		return $result;
	}

	function create_to($array_address)
	{
		return implode(",",$array_address);
	}
	
	function create_body()
	{
		$body = "This is a multi-part message in MIME format.\r\n\r\n";
		if(!$this->attachment) $boundary = $this->boundary;
		else $boundary = $this->subboundary;
		$body .= $this->message_bundle($boundary,$this->message);
		if($this->attachment) $body .= $this->attachment;
		$body .= "\r\n--Boundary-=".$this->boundary."--";
		return $body;
	}

	function create_header()
	{
		$header = "From: \"".$this->from_name."\" <".$this->from_mail.">\r\n";
		if(count($this->cc)) $header .= $this->create_address("CC",$this->cc);
		if(count($this->bcc)) $header .= $this->create_address("BCC",$this->bcc);
		$header .= "Reply-to: \"".$this->from_name."\" <".$this->from_mail.">\r\n";
		$header .= "X-Priority: ".$this->priority."\r\n";
		$header .= "X-Mailer: ".$this->mailer."\r\n";
		$header .= "MIME-Version: 1.0\r\n";
		
		if($this->attachment) $tipe_mail = "Mixed";
		else $tipe_mail = "alternative";
		$header .= "Content-Type: Multipart/".$tipe_mail.";\r\n";
		$header .= "\tboundary=\"Boundary-=".$this->boundary."\"\r\n\r\n";
		return $header;
	}

	function create_address($title,$array_address)
	{
		$temp = $title.": ";
		$temp .= @implode(", ",$array_address);
		$temp .= "\r\n";
		return $temp;
	}


	function message_bundle($boundary,$message)
	{
		if($boundary==$this->subboundary)
		{
			$result .= "--Boundary-=".$this->boundary."\r\n";
			$result .= "Content-Type: multipart/alternative; \r\n";
			$result .= "\tboundary=\"Boundary-=".$boundary."\"\r\n\r\n\r\n";
		}
		$result .= "--Boundary-=".$boundary."\r\n";
		$result .= "Content-Type: text/plain;\r\n";
		$result .= "\tcharset=\"".$this->CharSet."\"\r\n";
		$result .= "Content-Transfer-Encoding: quoted-printable\r\n\r\n";
		$result .= strip_tags(str_replace("<br>","\r\n",$message))."\r\n\r\n";
		$result .= "--Boundary-=".$boundary."\r\n";
		$result .= "Content-Type: text/html;\r\n";
		$result .= "\tcharset=\"".$this->CharSet."\"\r\n";
		$result .= "Content-Transfer-Encoding: quoted-printable\r\n\r\n";
		$result .= $message."\r\n\r\n";
		if($boundary==$this->subboundary) $result .= "--Boundary-=".$boundary."--\r\n\r\n";
		return $result;
	}
}

?>