<?php
header ("Expires: 0");    // Date in the past
header ("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
header ("Pragma: no-cache");                          // HTTP/1.0
$arr_hari = array("Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu");
$arr_bln = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");
$ip_address = $_SERVER['REMOTE_ADDR'];
$session_name = session_name();
$phpsessid = session_id();
//echo $phpsessid;
$today = date("d-m-Y");
$today_stamp = mktime(0,0,0,date("m"),date("d"),date("Y"));
$nextday_stamp = mktime(0,0,0,date("m"),date("d")+1,date("Y"));
$thisfile = basename($_SERVER['PHP_SELF']);//mengambil nama file
$template_name = str_replace(".php","",$thisfile); //$template_name=nama file templatenya
$php_logo = php_logo_guid();
$zend_logo = zend_logo_guid();

if(stristr($config_dir,"/")) $limit = "/";
else $limit = "\\";
$arr_fldr = @array_reverse(@explode($limit,$config_dir));
$app_fldr = $arr_fldr[1];
$pos = @strpos(strtolower($act_file),strtolower($app_fldr));
if(is_int($pos))
{
	$pos += strlen($app_fldr);
	$image_dir = str_replace(substr($act_file,$pos),"",$act_file)."/images";
}
//$session_dir = str_replace($limit.$arr_fldr[0],$limit."session",$config_dir);
//@session_save_path($session_dir);


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------

//--- db functions
function dbconnect()
{
	global $db_conn;
    @mysql_pconnect($db_conn[host],$db_conn[user],$db_conn[passwd]) or die("Host cannot found");
	@mysql_select_db($db_conn[db_name]) or die("Database not found");
}

function disconnect()
{
	@mysql_free_result($GLOBALS[result]);
	@mysql_close();
}

function clear(&$handler)
{
	@mysql_free_result($handler);
}
//--- end db funtions

//--- function coding
function encode($string)
{
	return base64_encode($string);
}

function decode($string)
{
	return base64_decode($string);
}
//--- end function coding

//----- size -----//
function size()
{
	global $row_count,$page_count;
	$page_count = 10;
	$row_count = 10;
}

function reverse_date($date,$from="-",$into="-")
{
	return @implode($into,array_reverse(@explode($from,$date)));
}

//----- Link with javascript -----//
function link_pro($parameter)
{
	$temp = "onmouseover=\"status='$parameter';return true;\" onmouseout=\"status='';return true;\"";
	return $temp;
}

function make_stamp($date,$time="")
{
	if($time)
	{
		$waktu = @explode(":",$time);
		$hour = $waktu[0];
		$minute = $waktu[1];
		$second = $waktu[2];
	}
	else
	{
		$hour = 0;
		$minute = 0;
		$second = 0;
	}
	$tanggal = @explode("-",$date);
	$day = $tanggal[0];
	$month = $tanggal[1];
	$year = $tanggal[2];
	return mktime($hour,$minute,$second,$month,$day,$year);
}

function pick_list_arr($array,$match_value='')
{
	if($array[select]) $select_field  = $array[select];
	if($array[from]) $table_name = $array[from];
	if($array[where]) $condition = $array[where];
	if($array[group_by]) $group_by = $array[group_by];
	if($array[order_by]) $order_by = $array[order_by];
	if($array[as_template]) $as_template = true;
	else $as_template = false;
	if($array[select_all]) $select_all = true;
	else $select_all = false;
	return pick_list($table_name,$select_field,$match_value,$order_by,$condition,$group_by,$as_template,$select_all);
}

function pick_list($table_name,$select_field,$match_value='',$order_by='',$condition='',$group_by='',$as_template=false,$select_all=false)
{
	$query = "select ".$select_field." from ".$table_name;
	if($condition) $query .= " where ".$condition;
	if($group_by) $query .= " group by ".$group_by;
	if($order_by) $query .= " order by ".$order_by;
//	echo $query;
	$result = @mysql_query($query);
	while($rows = @mysql_fetch_row($result))
	{
		$temp .= "<option value=\"".$rows[0]."\"";
		if($select_all) $temp .= " selected";
		elseif($as_template) $temp .= " \$selected_".$rows[0];
		elseif($rows[0]==$match_value and !$select_all) $temp .= " selected";
		$temp .= ">".$rows[1]."</option>\r\n";
	}
	return $temp;
}

function description_table($table)
{
	global $SENT_DATA;
	$query = "desc ".$table;
	$result = @mysql_query($query);
	while($rows = @mysql_fetch_object($result))
	{
		if(isset($SENT_DATA) and !isset($SENT_DATA[$rows->Field]) and $rows->Null) $fields[] = "NULL";
		else $fields[] = "\"\$".$rows->Field."\"";
	}
	$temp = "(".@implode(",",$fields).")";
	return $temp;
}

function description_edit($table,$skipped=null)
{
	global $SENT_DATA;
	$query = "desc $table";
	$result = @mysql_query($query);
	$skip_fields = @explode(",",$skipped);
	while($rows = @mysql_fetch_object($result))
	{
		if(!in_array($rows->Field,$skip_fields))
		{
			if(isset($SENT_DATA) and !isset($SENT_DATA[$rows->Field]) and $rows->Null) $fields[] = $rows->Field."=NULL";
			else $fields[] = $rows->Field."=\"\$".$rows->Field."\"";
		}
	}
	$temp = @implode(",",$fields);
	return $temp;
}

function mysql_fields_to_vars($result,$rows)
{
	for($i=0;$i<@mysql_num_fields($result);$i++)
	{
		$field_name = "\$".strtolower(@mysql_field_name($result,$i));
		$fields[] = $field_name;
		$values[] = $field_name."=\"".$rows[$i]."\";";
	}
	@eval("global ".@implode(",",$fields).";");
	@eval(@implode("",$values));
}

function hitung_data($query)
{
	$select_length = strlen("select");
	$from = substr(stristr($query,"from"),0,4);
	$from_pos = strpos($query,"$from ",$select_length);
	$select_fields = substr($query,$select_length,$from_pos - $select_length);
	if(stristr($query,"group by"))
	{
		$group = true;
		if(!stristr($query,"having")) $change_field = "1";
		else $change_field = $select_fields;
	}
	else $change_field = "count(*)";
	$query = str_replace($select_fields," $change_field ",$query);
	$order = substr(stristr($query,"order by"),0,strlen("order by"));
	$pos_order = strpos($query,$order);
	if($pos_order) $query = substr($query,0,$pos_order);
	//echo $query;
	$result = @mysql_query($query);
	if($group) $hasil = @mysql_num_rows($result);
	else
	{

		while($rows=mysql_fetch_row($result)){
			$hasil = $hasil + $rows[0];
		}
		//$temp = @mysql_fetch_row($result);
		//$hasil = $temp[0];
	}
	@mysql_free_result($result);
	return $hasil;
}

function optimize($tables)
{
	if(is_array($tables)) $tables = @implode(",",$tables);
	$query = "optimize table $tables";
	@mysql_query($query);
}

class drop_down
{
	var $select = null;
	var $from = null;
	var $where = null;
	var $group_by = null;
	var $having = null;
	var $order_by = null;
	var $as_template = false;
	var $select_all = false;
	var $first_value = null;
	var $num_row;
	//var $hasil_query = null;

	function build($match_value)
	{
		$query = "select ".$this->select;
		$query .= " from ".$this->from;
		if($this->where) $query .= " where ".$this->where;
		if($this->group_by) $query .= " group by ".$this->group_by;
		if($this->having) $query .= " having ".$this->having;
		if($this->order_by) $query .= " order by ".$this->order_by;
		//echo $query,"<br>";
		$result = @mysql_query($query);
		$this->num_row=@mysql_num_rows($result);
		while($rows =@mysql_fetch_row($result))
		{
			if(!$this->first_value) $this->first_value = $rows[0];
			$temp .= "<option value=\"".$rows[0]."\"";
			if($this->select_all) $temp .= " selected";
			elseif($this->as_template) $temp .= " \$selected_".$rows[0];
			elseif(!is_array($match_value) and $rows[0]==$match_value) $temp .= " selected";
			elseif(is_array($match_value) and in_array($rows[0],$match_value)) $temp .= " selected";
			$temp .= ">".$rows[1]."</option>\r\n";
		}
		return $temp;
	}
}
?>
