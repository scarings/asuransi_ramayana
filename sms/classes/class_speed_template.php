<?
class speed_template
{

var $files = array();
var $var_names = "";
var $blok = array();
var $stack = array();
var $root = "templates";
var $var_start = "{";
var $var_end = "}";
var $start_blok = "<!#";
var $end_blok = "<!#/";
var $close_blok = "#!>";
//var $div_blok = "<batas>";
var $counter;
//var $max_counter = "";

function speed_template($root=null) 
{
	if(!is_null($root)) $this->root = $root; //inisialisasi lokasi direktori template
}

function register($handler,$file_name="") //untuk mengubah .php ke .htm
{
	if(!$file_name) $file_name = $handler.".htm";  
	if($this->root) $file_name = $this->root."/".$file_name;
	$this->files[$handler] = " ".@trim(@implode("",@file($file_name))); //membuat array pada tiap barisnya kemudian dijadikan satu string
	if(stristr($this->files[$handler],$this->start_blok)) $this->register_blok($handler); //mencari apakah ada start_blok nya,jika ada diregister
}

function search_blokname($string,$start_point) //untuk mencari blok <!#
{
	$start = @strpos($string,$this->start_blok,$start_point);
	$end = @strpos($string,$this->close_blok,$start);
	if(is_numeric($start) and is_numeric($end)) //berarti ada start dan end block
	{
		$start += @strlen($this->start_blok);
		$blok_name = substr($string,$start,$end-$start); //mengambil nama yang diblok
		$posisi_end_blok = @strpos($string,$this->end_blok.$blok_name.$this->close_blok,$end);
		if($posisi_end_blok) return $blok_name;
		else return false;
	}
	else return false;
}

function register_blok($handler,$content="") 
{
	if(!$content) $string = $this->files[$handler];
	else $string = $content;
	while($blok_name = $this->search_blokname($string,$end))
	{
		$this->blok_content($handler,$blok_name);
		$end = @strpos($string,$this->end_blok.$blok_name.$this->close_blok,$end) + strlen($this->end_blok.$blok_name.$this->close_blok);
	}
}

function blok_content($handler,$blok_name)
{
	$start_tag = $this->start_blok.$blok_name.$this->close_blok; //nama awal blok 
	$end_tag = $this->end_blok.$blok_name.$this->close_blok; //nama akhir blok
	$start_pos = @strpos($this->files[$handler],$start_tag) + @strlen($start_tag); //posisi awal blok
	$end_pos = @strpos($this->files[$handler],$end_tag); //posisi akhir blok
	$content = @substr($this->files[$handler],$start_pos,$end_pos-$start_pos);  //isi blok
	if(stristr($content,$this->start_blok)) //jika dalam blok ada blok lagi
	{
		$this->register_blok($handler,$content);
		$end_pos = @strpos($this->files[$handler],$end_tag);
		$content = @substr($this->files[$handler],$start_pos,$end_pos-$start_pos);
	}
	$old = $start_tag.$content.$end_tag;
	$new = $this->var_start.$blok_name.$this->var_end;
	//echo $this->files[$handler];
	$this->files[$handler] = @str_replace($old,$new,$this->files[$handler]); //mengubah kedalam bentuk {nama_blok}
	//echo $this->files[$handler];
	/*$temp = @explode($this->div_blok,$content); //nggak jelas kamsudnya
	$total = @count($temp);
	if(@$total>1)
	{
		$this->blok[$handler][$blok_name] = $temp;
		$tshis->max_counter[$handler][$blok_name] = $total;
		$this->counter[$handler][$blok_name] = 0;
	}
	else*/ 
	$this->blok[$handler][$blok_name] = $content;
}

function search($handler,$special=FALSE,$send_str="")
{
	if($special AND $send_str) $string = $send_str;
	else $string = $this->files[$handler];
	while($start = @strpos($string,$this->var_start,$start) AND $end = @strpos($string,$this->var_end,$start))
	{
		$start += @strlen($this->var_start);
		while($temp = @strpos($string,$this->var_start,$start) AND $temp < $end)	$start = $temp+strlen($this->var_start);
		$temp = @substr($string,$start,$end-$start);
		if(!$temp) continue;
		if(stristr($temp," ") or stristr($temp,"\r\n")) continue;
		if($special) global $$temp;
		else $this->var_names[$handler][] = $temp;
		$string = str_replace($this->var_start.$temp.$this->var_end,$$temp,$string);
	}
	if($special) return $string;
	else return $this->var_names[$handler];
}

function parse($handler,$string="")
{
	if(!$string) $direct = TRUE;
	if(!$string AND is_array($this->var_names[$handler]))
	{
		for(@reset($this->var_names[$handler]);$var_name = @current($this->var_names[$handler]);@next($this->var_names[$handler]))
		{
			global $$var_name;
			$string = @str_replace($this->var_start.$var_name.$this->var_end,$$var_name,$string);
		}
	}
	else
	{
		if(!$string) $string = $this->files[$handler];
		$string = $this->search($handler,1,$string);
	}

	if($direct) $this->files[$handler] = $string;
	else return $string;
}

function push($handler,$blok_name)
{
	if(is_array($this->blok[$handler][$blok_name]))
	{
		$counter = $this->counter[$handler][$blok_name];
		$template = $this->blok[$handler][$blok_name][$counter];
		$counter++;
		if($counter == $this->max_counter[$handler][$blok_name]) $this->counter[$handler][$blok_name] = 0;
		else $this->counter[$handler][$blok_name] = $counter;
	}
	else $template = $this->blok[$handler][$blok_name];
	$this->stack[$handler][$blok_name] .= $this->parse($handler,$template);
}

function pop($handler,$blok_name)
{
	$temp = $this->stack[$handler][$blok_name];
	$this->stack[$handler][$blok_name] = "";
	$this->counter[$handler][$blok_name] = 0;
	return $temp;
}

function finish_loop($handler,$blok_name) //berhenti loop sehabis di push
{
	$old = $this->var_start.$blok_name.$this->var_end;
	$new = $this->pop($handler,$blok_name);
	$this->files[$handler] = @str_replace($old,$new,$this->files[$handler]);
}

function return_template($handler)
{
	return $this->files[$handler];
}

function print_template($handler) //menulis 
{
	echo $this->files[$handler];
}

function show_blok($handler,$blok_name) //utk menampilkan blok yg tlah dtandai
{
	$this->push($handler,$blok_name);
	$this->finish_loop($handler,$blok_name);
}

}
?>
