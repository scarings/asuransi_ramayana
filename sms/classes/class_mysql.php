<?
class dbmysql
{

// format arr_ref adalah key awal adalah nama tabel yang direfer 
// dan valuenya berupa array tabel yang merefer dimana array tabel yang merefer
// berisi key adalah nama tabel yang merefer dan valuenya adalah nama_field yang direfer->nama_field referer
// atau valu berisi array jika dalam satu tabel referer ada > 1 field yang merefer field di tabel yg direfer
// kemudian ditambah->R jika restricted atau C-> jika cascade hanya pada saat hapus def: restricted
// contoh :
//--- ada 3 tabel table A,B, dan C. Tabel A direfer oleh B dan C. Field yang direfer adalah id,
//--- pada tabel B yg merefer adalah field id_pengirim & pd tbl C adl field id_pengguna
//--- maka bentuk array referernya =>
//--- $arr_ref[A] = array('B'=>'id->id_pengirim','C'=>'id->id_pengguna');
//--- jika di tbl B ada 2 field yg merefer yaitu id_pengirim & id_penerima
//--- maka bentuk array referernya =>
//--- $arr_ref[A] = array('B'=>array('id->id_pengirim','id->id_penerima'),'C'=>'id->id_pengguna');

var $arr_ref = array();
var $err_entegrity = false;
var $cascade = false;
var $is_delete = false;

function proses($query,$buffered)
{
	// untuk insert lakukan insert seperti biasa OK
	if($buffered) $result = @mysql_query($query);
	else $result = @mysql_unbuffered_query($query);
	return $result;
}

function check_integrity($table,$tipe)
{
	// fungsi untuk mengecek integritas data / reference
	if(!$this->arr_ref[$table]) return true; // jika tidak di referensi maka anggap lolos
	else
	{
		$hasil = true;
		while(list($tbl_referer,$value)=@each($this->arr_ref[$table]))
		{
			if(is_array($value))
			{
				while(list(,$nilai)=@each($value))
				{
					$temp = @explode("->",$nilai);
					if($temp[2] != "C")
					{
						$hasil = false;
						break;
					}
				}
			}
			else
			{
				$temp = @explode("->",$value);
				if($temp[2] != "C")
				{
					$hasil = false;
					break;
				}
			}
		}
		if($hasil) $this->cascade = true;
		return $hasil;
	}
}

function proses_cascade($table,$query)
{
	// fungsi untuk memproses cascade
	if($this->is_delete)
	{
	
	}
	else
	{
		while(list)
	}
}

function query($query)
{
	// fungsi ini adalah induk fungsi yang ada 	dengan melihat
	$query = str_replace("\r","",$query);
	$query = str_replace("\n","",$query);
	while(stristr($query,"  ")) $query = str_replace("  "," ",$query);
	$temp = @explode(" ",$query);
	$tipe = strtolower($temp[0]);
	// -- jika untuk query select harus bertipe buffered agar dapat di fetch
	if($tipe=="select") $buffered = true;
	else $buffered = false;
/* sementara tutup dulu untuk kejar tayang :)
	//--- jika untuk query hapus perlu check integritas !!!
	if($tipe=="delete")
	{
		$table = $temp[2];
		$this->is_delete = true;
		$all_ok = $this->check_integrity($table);
	}
	elseif($tipe=="update")
	{
		$table = $temp[1];
		$all_ok = true;
		$this->cascade = true;
	}
	else $all_ok = true;
*/	
	$all_ok = true;
	if($all_ok) $hasil = $this->proses($query,$buffered);	
	else
	{
		$this->err_entegrity = true;
		return false;
	}
	
	if($hasil and $this->cascade) $this->proses_cascade($table,$query);
	return $hasil;
}

}
?>