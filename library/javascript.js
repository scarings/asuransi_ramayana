function ngepop2(path_url){
  mailform=window.open(path_url,'108','toolbar=no;location=no,width=700,height=400');
}
function ngeprint(path_url){
  mailform=window.open(path_url,'Print','toolbar=no;location=no,scrollbars,width=700,height=700');
}
function ngepop(path_url){
  mailform=window.open(path_url,'108','toolbar=no;location=no,scrollbars,width=500,height=400');
}
function img(msg){
  mailform=window.open('procimg.php?no='+msg,'ShowImage'+msg,'toolbar=no;location=no,width=368,height=325');
}


function checklogin()
{
	var retval = "";
	with(document.form)
	{	
		retval = nama.value + "|" + passwd.value;
	}
	window.returnValue = retval;
	event.returnValue = false;
	window.close();
}

function login(strwarning,coded)
{
	var temp = "";
	temp = showModalDialog('login_form.php?error='+strwarning,'','dialogWidth=200pt;dialogHeight=220pt;center=yes;help=no;scroll=no');
	if(!temp) history.back();
	else window.location.replace('login_temp.php?param='+temp+'&coded='+coded);
}

function banding()
{
	var old_password = document.getElementById('old_password');
	var new_password = document.getElementById('new_password');
	var confirm_password = document.getElementById('confirm_password');
	if(new_password.value!=confirm_password.value)
	{
		alert('Password baru dan Konfirmasi password baru tidak sama.');
		confirm_password.value = '';
		confirm_password.focus();
		event.returnValue = false;
	}
	else
	if(old_password.value==new_password.value)
	{
		alert('Password baru sama dengan password lama.');
		confirm_password.value = new_password.value = '';
		new_password.focus();
		event.returnValue = false;
	}
	else
	{
		var retval = "";
		retval = old_password.value + "|" + new_password.value;
		window.returnValue = retval;
		event.returnValue = false;
		window.close();
	}
}

function ubah_password(strwarning)
{
	var temp = "";
	temp = showModalDialog('change_form.php?error='+strwarning,'','dialogWidth=300pt;dialogHeight=250pt;center=yes;help=no;scroll=no');
	if(!temp) history.back();
	else window.location.replace('change_temp.php?param='+temp);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' harus berisi alamat email.\n';
      } else if (test!='R') {
        if (isNaN(val)) errors+='- '+nm+' harus berisi angka.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (val<min || max<val) errors+='- '+nm+' harus berisi angka antara '+min+' dan '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' harus diisi.\n'; }
  } if (errors) alert('Terdapat kesalahan :\n'+errors);
  document.MM_returnValue = (errors == '');
}

function ubah_status()
{
	var args = ubah_status.arguments;
	var form_name = args[0];
	var nilai = args[1];
	for(n=2;n<args.length;n++)
	{
	    fields = document.forms[form_name].elements[args[n]];
		fields_cnt  = (typeof(fields.length) != 'undefined') ? fields.length : 0;
		for(var i=0;i<fields_cnt;i++) fields[i].disabled = !nilai;
		fields.disabled = !nilai;
	}
}

function set_window()
{
	var scr_width = window.screen.width;
	var scr_height = window.screen.height;
	var arg_cnt = arguments.length;
	if(arg_cnt > 1)
	{
		wnd_width = arguments[0];
		wnd_height = arguments[1];
	}
	else
	{
		wnd_width = (scr_width * arguments[0]) /100;
		wnd_height = (scr_height * arguments[0]) /100;
	}
	pos_left = (scr_width - wnd_width) / 2;
	pos_top = (scr_height - wnd_height) / 2;
	window.resizeTo(wnd_width,wnd_height);
	window.moveTo(pos_left,pos_top);
	window.focus();
}

function drop_down(target,index,arr_name,def_val)
{
	targetfld = document.getElementById(target);
	dict = eval("rel_"+arr_name+"['"+index+"']");
	for(i=targetfld.options.length;i>=0;i--)
		targetfld.options[i] = null;
	if(dict != 'undefined' && dict != null && index != null)
	{
		temp = dict.split(',');
		new_arr = eval(arr_name);
		if(def_val && def_val.indexOf('|'))
		{
			banding = def_val.split('|');
		}
		else banding[0] = def_val;
		for(i=0;i<temp.length;i++)
		{
			targetfld.options[i] = new Option(new_arr[temp[i]]);
			targetfld.options[i].value = temp[i];
			for(j=0;j<banding.length;j++)
			{
				if(banding[j]== temp[i])
				{
					targetfld.options[i].selected = true;
					break;
				}
			}
//			if(temp[i] == def_val) targetfld.options[i].selected = true;
		}
	}
	else 
	{
		targetfld.options[0] = new Option(" <-- no data --> ");
		targetfld.options[0].value = "";
		targetfld.options[0].selected = true;
	}
}

function pressed(type)
{
	if(type=='N')
	{
		if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;
	}
	else if(type=='A')
	{
		if( (event.keyCode > 32 && event.keyCode < 47) || (47 < event.keyCode && event.keyCode < 65))  event.returnValue = false;
	}
	else
	{
		if(event.keyCode == 34 || event.keyCode == 92) event.returnValue = false;
	}
}

function number_format()
{
	if (event.keyCode < 46 || event.keyCode > 57 || event.keyCode == 47) event.returnValue = false;
	else
	{
		el = event.srcElement;
		number = el.value;
		while (number.indexOf(',') != -1) number = number.replace(',','');
		if(event.keyCode == 46) number += '.';
		else number += event.keyCode - 48;
		arrNumber = number.split('.');
		tot_front = el.maxLength - 2;
		all_length = el.maxLength+1;
		if(arrNumber.length == 1 && number.length >tot_front) number = 'salah';
		else if(arrNumber.length > 1)
		{
			ext = '' + arrNumber[1];
			if(number.length > all_length) number = 'salah';
			else if(ext.length > 2) number = 'salah';
		}
		
		if(isNaN(number)) event.returnValue = false;
		else
		{
			el.value = commaSplit(number);
			event.returnValue = false;
		}
	}
}

function commaSplit(srcNumber)
{
	while (srcNumber.indexOf(',') != -1) srcNumber = srcNumber.replace(',','');
	var rxSplit = new RegExp('([0-9])([0-9][0-9][0-9][,.])');
	var arrNumber = srcNumber.split('.');
	arrNumber[0] += '.';
	do {
		arrNumber[0] = arrNumber[0].replace(rxSplit, '$1,$2');
	} while (rxSplit.test(arrNumber[0]));
	if (arrNumber.length > 1) return arrNumber.join('');
	else return arrNumber[0].split('.')[0];
}

function left_time(left_time)
{
	var minute = 60;
	var hour = 60 * minute;
	var day = 24 * hour;
	num_day_left = parseInt(left_time / day);
	num_hour_left = parseInt((left_time - (num_day_left * day)) / hour);
	num_minute_left = parseInt((left_time - (num_day_left * day) - (num_hour_left * hour)) / minute);
	num_second_left = parseInt(left_time - (num_day_left * day) - (num_hour_left * hour) - (num_minute_left * minute));
	if(num_day_left <= 0 && num_hour_left <= 0 && num_minute_left <= 0 && num_second_left <= 0)
	{
		document.location.replace('time_up.htm');
	}
	dispTime = "";
	num_day_left = "" + num_day_left;
	num_hour_left = "" + num_hour_left;
	num_minute_left = "" + num_minute_left;
	num_second_left = "" + num_second_left;
	if(num_day_left != "0")
	{
		if(num_day_left.length==1) num_day_left = "0" + num_day_left;
		dispTime += num_day_left + ' hari, ';
	}
	if(num_hour_left != "0" || (num_day_left != "0"))
	{
		if(num_hour_left.length==1) num_hour_left = "0" + num_hour_left;
		dispTime += num_hour_left + ' jam : ';
	}
	if(num_minute_left != "0" || (num_day_left != "0") || (num_hour_left != "0"))
	{
		if(num_minute_left.length==1) num_minute_left = "0" + num_minute_left;
		dispTime += num_minute_left + ' menit : ';
	}
	if(num_second_left.length==1) num_second_left = "0" + num_second_left;
	dispTime += num_second_left + ' detik';
	left_time -= 1;
	if (document.layers) document.layers.sisawaktu.value = dispTime;
	else if (document.all)	sisawaktu.innerHTML = dispTime;
	setTimeout("left_time("+ left_time +")", 1000);
}

var marked_row = new Array;

function setPointer(theRow, theRowNum, theAction, theDefaultColor, thePointerColor, theMarkColor)
{
    var theCells = null;

    // 1. Pointer and mark feature are disabled or the browser can't get the
    //    row -> exits
    if ((thePointerColor == '' && theMarkColor == '')
        || typeof(theRow.style) == 'undefined') {
        return false;
    }

    // 2. Gets the current row and exits if the browser can't get it
    if (typeof(document.getElementsByTagName) != 'undefined') {
        theCells = theRow.getElementsByTagName('td');
    }
    else if (typeof(theRow.cells) != 'undefined') {
        theCells = theRow.cells;
    }
    else {
        return false;
    }

    // 3. Gets the current color...
    var rowCellsCnt  = theCells.length;
    var domDetect    = null;
    var currentColor = null;
    var newColor     = null;
    // 3.1 ... with DOM compatible browsers except Opera that does not return
    //         valid values with "getAttribute"
    if (typeof(window.opera) == 'undefined'
        && typeof(theCells[0].getAttribute) != 'undefined') {
        currentColor = theCells[0].getAttribute('bgcolor');
        domDetect    = true;
    }
    // 3.2 ... with other browsers
    else {
        currentColor = theCells[0].style.backgroundColor;
        domDetect    = false;
    } // end 3

    // 4. Defines the new color
    // 4.1 Current color is the default one
    if (currentColor == ''
        || currentColor.toLowerCase() == theDefaultColor.toLowerCase()) {
        if (theAction == 'over' && thePointerColor != '') {
            newColor              = thePointerColor;
        }
        else if (theAction == 'click' && theMarkColor != '') {
            newColor              = theMarkColor;
            marked_row[theRowNum] = true;
        }
    }
    // 4.1.2 Current color is the pointer one
    else if (currentColor.toLowerCase() == thePointerColor.toLowerCase()
             && (typeof(marked_row[theRowNum]) == 'undefined' || !marked_row[theRowNum])) {
        if (theAction == 'out') {
            newColor              = theDefaultColor;
        }
        else if (theAction == 'click' && theMarkColor != '') {
            newColor              = theMarkColor;
            marked_row[theRowNum] = true;
        }
    }
    // 4.1.3 Current color is the marker one
    else if (currentColor.toLowerCase() == theMarkColor.toLowerCase()) {
        if (theAction == 'click') {
            newColor              = (thePointerColor != '')
                                  ? thePointerColor
                                  : theDefaultColor;
            marked_row[theRowNum] = (typeof(marked_row[theRowNum]) == 'undefined' || !marked_row[theRowNum])
                                  ? true
                                  : null;
        }
    } // end 4

    // 5. Sets the new color...
    if (newColor) {
        var c = null;
        // 5.1 ... with DOM compatible browsers except Opera
        if (domDetect) {
            for (c = 0; c < rowCellsCnt; c++) {
                theCells[c].setAttribute('bgcolor', newColor, 0);
            } // end for
        }
        // 5.2 ... with other browsers
        else {
            for (c = 0; c < rowCellsCnt; c++) {
                theCells[c].style.backgroundColor = newColor;
            }
        }
    } // end 5

    return true;
} // end of the 'setPointer()' function

function shake(n) {
if (parent.moveBy) {
for (i = 10; i > 0; i--) {
for (j = n; j > 0; j--) {
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
parent.moveBy(0,i);
parent.moveBy(i,0);
parent.moveBy(0,-i);
parent.moveBy(-i,0);
         }
      }
   }
}

function validate_date(element)
{
	el = document.forms[0].elements[element];
	var test = new Date(el[1].value+'-'+el[0].value+'-'+el[2].value);
	tgl = parseInt(test.getDate());
	bln = parseInt(test.getMonth());
	tgl--;
	el[0][tgl].selected = true;
	el[1][bln].selected = true;
	el[2].value = test.getFullYear();
}

function groupHeaderClick(row, level) {
    var collapseText = "Collapse";
    var expandText = "Expand";

    var rowID = row.id;
    var rows = row.parentNode.rows;
    // IE4: var rows = row.parentElement.rows;
    // revert collapse/expand symbol
    var cell = row.cells[row.cells.length-1];
    var span = cell.colSpan;
    var img = cell.childNodes[0];
    // IE4: var img = cell.children(0);     // NOTE: index=0 not 1
    var disp;
    var n = img.alt.indexOf(collapseText);
    var collapse;
    if (n >= 0) {
        img.alt = expandText;
        n = img.src.indexOf("expand.gif");
        img.src = img.src.substring(0, n) + "collapse.gif";
        disp = "none";
        collapse = true;
    } else {
        img.alt = collapseText;
        n = img.src.indexOf("collapse.gif");
        img.src = img.src.substring(0, n) + "expand.gif";
        // IE4: disp = "block";
        disp = "";
        collapse = false;
    }
    // expand/collapse all rows until the next group is reached
    var i = row.rowIndex;
    var flag = true;
    var rowLevel = level;
    var isHeader;
    while (flag) {
        i++;
        flag = (i < rows.length);
        if (flag) {
            // IE4: row = rows(i);
            row = rows[i];
            // determine if we reached another group header
            isHeader = (row.id.length > 0 && row.id.charAt(0) == 'g')
            if (isHeader) {
                // reached another groupheader..determine its level
                rowLevel = (span - row.cells[row.cells.length-1].colSpan) + 1;
            }

            flag = (isHeader && rowLevel > level)
                || (!isHeader && rowLevel >= level);
            if (flag) {
                if (isHeader) {
                    if (row.style.display == "none") {
                        // IE4: row.style.display = "block";
                        row.style.display = "";
                    } else {
                        row.style.display = "none";
                    }
                    if (!collapse) {
                        // determine if row is collapsed or expanded
                        cell = row.cells[row.cells.length - 1];
                        img = cell.children(0);
                        n = img.alt.indexOf("Collapse");
                        if (n >= 0) {
                            // IE4: disp = "block";
                            disp = "";
                        } else {
                            disp = "none";
                        }
                    }
                } else {
                    row.style.display = disp;
                }
            } else {
               flag = (rowLevel > level);
            }
        }
    }
}



var IE = (document.all ? true : false);

function selectValue(fld, value)
{
    if (fld != null) {
        for (var i = 0; i < fld.options.length; i++) {
            if (fld[i].value == value) {
                fld.selectedIndex = i;
                break;
            }
        }
    }
}